import { Routes } from '@angular/router';
import { EstadisticaComponent } from '../ingreso-egreso/estadistica/estadistica.component';
import { IngresosEgeresosComponent } from '../ingreso-egreso/ingresos-egeresos/ingresos-egeresos.component';
import { DetalleComponent } from '../ingreso-egreso/detalle/detalle.component';


export const DashboardRoutes: Routes = [

  { path: '', component: EstadisticaComponent },
  { path: 'ingreso-egreso', component: IngresosEgeresosComponent},
  { path: 'detalle', component: DetalleComponent}

];
